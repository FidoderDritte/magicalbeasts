# Project 120 - Magical Beasts von Laura Frei

## GUI Design Prozess Vorgehensweise
![UI/UX Process](https://www.softkraft.co/static/34b47899962e1889f29b613d479f43bb/c74de/7-step-process-of-UI-design.webp)

### 1- Design Ziele setzen
Wir haben bewusst einfache und leicht verständliche Designziele festgelegt, um sicherzustellen, dass die Webseite für alle Zielgruppen zugänglich ist, einschließlich älterer Hexen und Zauberer, die möglicherweise weniger vertraut mit der Technologie sind.

### 2- User Research 
Da die Zielgruppe der Hexen und Zauberer mit der Technologie nicht vertraut ist, haben wir uns auf die Aussagen von "Muggels" (Nicht-Magie-Anwender) verlassen, um Einblicke in die Bedürfnisse und Erwartungen der Benutzer zu gewinnen.
### 3- Einfühlen in den User und Userstories formulieren

Wir haben uns intensiv mit der Zielgruppe auseinandergesetzt und darüber nachgedacht, wie wir sowohl junge Hexen und Zauberer als auch ältere Muggel ansprechen können. Dabei haben wir darauf geachtet, dass die Webseite einfach und benutzerfreundlich bleibt und keine fortgeschrittenen Kenntnisse im Umgang mit dem Internet erfordert.

**User Stories**
- Plattform für Magical Beasts: Als Zauber will ich eine Plattform auf welcher ich magische Tierwesen zur Adoption ausgeschrieben sind.
- Filterfunktion für Shop: Im Shop will ich die Produkte filtern können.
- Adoption und Fortschritt: Als Zauberer will ich einen Adoptions antrag stellen können und dessen fortschritt visuell einsehen können.
- Bewertungen im Shop: Als Zauberer will ich eine Möglichkeit haben eine Bewertung der angebotenen Produkte zuhaben.
### 4-Design Konzept auswählen
Unser Designkonzept wurde von der magischen Welt von Hogwarts inspiriert, um eine vertraute und einladende Atmosphäre zu schaffen. Die Auswahl von natürlichen Farben unterstreicht die Verbundenheit zur Natur.

**Gewähltes Farbschema**

![img.png](img.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)

### 5-Designprototyp: 
Der Designprototyp wurde direkt in das Projekt integriert, um sicherzustellen, dass die Benutzerfreundlichkeit und die Umsetzung unserer Designideen von Anfang an gewährleistet sind. Dies spart Zeit und Ressourcen in der Entwicklungsphase.

### 6-Testing: 
Wir haben die Webseite ausführlich getestet, um sicherzustellen, dass sie sowohl für junge Hexen und Zauberer als auch für ältere Muggel leicht verständlich und benutzerfreundlich ist. Die Testergebnisse halfen uns, Schwachstellen zu identifizieren und Verbesserungen vorzunehmen.

### 7-Verbessern und Aktualisieren: 
Unser Designprozess endet nicht mit der Veröffentlichung der Webseite. Wir haben bereits geplante Verbesserungen in Aussicht, um sicherzustellen, dass die Webseite kontinuierlich den sich verändernden Bedürfnissen und Erwartungen der Benutzer gerecht wird. Eine dieser geplanten Verbesserungen besteht darin, dass Benutzer die Möglichkeit haben, ein persönliches Profil anzulegen. Dieses Profil ermöglicht es den Nutzern, den Status ihrer Lieferungen zu verfolgen und gleichzeitig eine Übersicht darüber zu erhalten, welche "Magical Beasts" sie bereits adoptiert haben.

Diese Erweiterungen sind Teil unseres Engagements, sicherzustellen, dass die Webseite stets auf dem neuesten Stand bleibt und den Bedürfnissen unserer Benutzer in jeder Phase ihres Besuchs gerecht wird. Wir sind bestrebt, eine ständig verbesserte Benutzererfahrung zu bieten, um die Faszination für die magische Welt lebendig zu halten und den Nutzern ein Höchstmaß an Komfort und Mehrwert zu bieten.



## Erklärungen zum gewählten Design
Das gewählte Designkonzept wurde sorgfältig ausgewählt, um sicherzustellen, dass sowohl 
junge Hexen und Zauberer als auch diejenigen, die weniger vertraut mit dem Internet sind, 
sich schnell auf der Webseite zurechtfinden können. Unser Hauptziel war es, ein Design zu schaffen, 
das intuitiv und benutzerfreundlich ist, ohne übermäßig kompliziert oder modern zu wirken.

Wir haben uns bewusst dafür entschieden, die "Success Criteria" nicht zu hoch anzusetzen. 
Unser Hauptkriterium für Erfolg war es, dass Hexen und Zauberer, selbst wenn sie wenig Erfahrung 
mit der Internetnutzung haben, mühelos auf die Webseite zugreifen können. Darüber hinaus sollte
die Webseite auch für die Entdeckung neuer "Magical Beasts" geeignet sein.

## Vorgehen begründung
Die Methode, den Designprozess in sieben Schritte aufzuteilen, hat sich als äusserst effektiv erwiesen, um sicherzustellen, dass die richtigen Fragen gestellt werden und eine Webseite entsteht, die gleichermaßen ansprechend und benutzerfreundlich ist. Auf diese Weise wird die Möglichkeit geschaffen, die Faszination der dargestellten Welt auf eine Weise zu präsentieren, die für eine vielfältige Zielgruppe leicht zugänglich ist und unterschiedlichen Bedürfnissen gerecht wird.

**Benutzerfreundlichkeit:** In der Gestaltung wird besonderes Augenmerk auf die Benutzerfreundlichkeit gelegt, um sicherzustellen, dass sowohl Neueinsteiger als auch erfahrene Nutzer problemlos durch die Benutzeroberfläche navigieren können. Klar verständliche Beschriftungen und intuitives Feedback sind dabei Schlüsselelemente.

**Ästhetik:** Die Webseiten werden nicht nur funktional, sondern auch ästhetisch ansprechend gestaltet und harmonieren dabei stets mit der Markenidentität. Sorgfältig ausgewählte Farbpaletten, Schriftarten und Grafiken sorgen für ein durchgängiges und ansprechendes Erscheinungsbild.

**Konsistenz:** Die Aufrechterhaltung von Konsistenz ist von entscheidender Bedeutung für eine positive Benutzererfahrung. Jede Seite des Designs folgt einem einheitlichen Muster, sei es bei der Menüführung, der Schriftartwahl oder der Verwendung von Farben.

**Benutzerfeedback:** Feedback-Mechanismen werden integriert, die den Nutzern klare Signale über ihre getätigten Aktionen geben. Dies trägt zur Transparenz und Verständlichkeit der Webseite bei und erhöht die Interaktivität.

**Fehlervermeidung:** Ein besonderes Augenmerk wird darauf gelegt, sicherzustellen, dass Benutzer keine fehlerhaften Eingaben machen können. Dies wird durch die gezielte Beschränkung des Zugangs zu bestimmten Seiten und Funktionen erreicht sowie durch die Schaffung klarer und leicht verständlicher Pfade für gezielte Aktionen.

**Verringerung der Belastung des Kurzzeitgedächtnisses:** Das Design zielt darauf ab, Benutzern die Notwendigkeit zu ersparen, unnötige Informationen im Kurzzeitgedächtnis behalten zu müssen. Zum Beispiel wird es Benutzern ermöglicht, Bücher in den Warenkorb zu legen und diese dort auf übersichtliche Weise zu verwalten.


# How to get the Code running: 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.0.

## Primeng
The Components used in this Project are from the [Primeng](https://primeng.org/installation) Libary
`npm install primeng`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.


