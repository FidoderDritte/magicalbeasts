import { Component, OnInit } from '@angular/core';
import { DropdownChangeEvent } from 'primeng/dropdown';
import { DataViewLayoutOptions } from 'primeng/dataview'
import { DataViewModule } from 'primeng/dataview';


@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],
})
export class ShopComponent implements OnInit {
  layout: 'list' | 'grid' = 'list';
  selectedCategory: string | null = null;
  originalProducts: any[] = [];
  sortOptions: any[] = [];
  sortKey: string | null = null;

  products: any[] = [
    {
      name: 'Potion N.07',
      price: 19.99,
      category: 'Potions',
      rating: 4.5,
      inventoryStatus: 'IN STOCK',
      image: 'https://th.bing.com/th/id/OIP._jYxQhgWTCvAItO1TL74BQAAAA?w=214&h=214&c=7&r=0&o=5&pid=1.7',
    },
    {
      name: 'Potion N.03',
      price: 29.99,
      category: 'Potions',
      rating: 3.8,
      inventoryStatus: 'IN STOCK',
      image: 'https://th.bing.com/th/id/OIP.UvRuhg6moF3rQaP96YfaNAHaJt?pid=ImgDet&rs=1',
    },
    {
      name: 'Potion N.17',
      price: 29.99,
      category: 'Potions',
      rating: 4.8,
      inventoryStatus: 'OUT OF STOCK',
      image: 'https://cdn3d.iconscout.com/3d/premium/thumb/potion-bottle-5435769-4544510.png',
    },
    {
      name: 'Potion N.09',
      price: 29.99,
      category: 'Potions',
      rating: 2.8,
      inventoryStatus: 'LOW STOCK',
      image: 'https://th.bing.com/th/id/OIP.7ePtvADEEh0kX-u7vBjVewAAAA?pid=ImgDet&w=450&h=450&rs=1',
    },
    {
      name: 'Potion N.07',
      price: 19.99,
      category: 'Feed',
      rating: 4.5,
      inventoryStatus: 'IN STOCK',
      image: 'https://www.petplanet.co.uk/image/500x500/20_68981_3_1593000156_430aaf.jpg',
    },
  ];

  categoryOptions: any[] = [
    { label: 'All', value: null },
    { label: 'Potions', value: 'Potions' },
    { label: 'Feed', value: 'Feed' },
  ];

  constructor() {}

  ngOnInit() {
    this.originalProducts = [...this.products];
    this.sortOptions = [
      { label: 'Sort By Name', value: 'name' },
      { label: 'Sort By Price (Low to High)', value: 'price-asc' },
      { label: 'Sort By Price (High to Low)', value: 'price-desc' },
      { label: 'Sort By Rating (High to Low)', value: 'rating-desc' },
    ];
  }

  /* GetSeverityColor(product: any): string {
    if (this.getSeverity(product) === 'success') {
      return '#C6BF5F'; // Custom color for success
    } else if (this.getSeverity(product) === 'warning') {
      return '#DC965A'; // Custom color for warning
    } else {
      // Return a default color for other severities or fallback
      return '#000'; // You can change this to your desired default color
    }
  } */

  toggleCartStatus(product: any) {
    product.addedToCart = !product.addedToCart;
  }

  getSeverity(product: any) {
    switch (product.inventoryStatus) {
      case 'IN STOCK':
        return 'success';
      case 'OUT OF STOCK':
        return '';
      case 'LOW STOCK':
        return 'warning';
      default:
        return '';
    }
  }

  filterProductsByCategory() {
    console.log('Selected Category:', this.selectedCategory);
    if (this.selectedCategory) {
      this.products = this.originalProducts.filter(
        (product) => product.category === this.selectedCategory
      );
    } else {
      this.products = [...this.originalProducts];
    }
    console.log('Filtered Products:', this.products);
    this.layout = 'list';
  }



  onSortChange($event: DropdownChangeEvent) {
    const { value } = $event;
    this.sortKey = value;

    switch (value) {
      case 'price-asc':
        this.products.sort((a, b) => a.price - b.price);
        break;
      case 'price-desc':
        this.products.sort((a, b) => b.price - a.price);
        break;
      case 'rating-desc':
        this.products.sort((a, b) => b.rating - a.rating);
        break;
      default:
        // Default sorting logic (e.g., by name)
        this.products.sort((a, b) => a.name.localeCompare(b.name));
        break;
    }
  }
  isProductInCart(product: any): boolean {
    return product.addedToCart;
  }



}

