import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

interface HogwartsHouse {
  name: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {
  Form: FormGroup;
  value!: string;

  hogwartsHouse: HogwartsHouse[] | undefined;
  selectedHouse: HogwartsHouse | undefined;

  constructor(private fb: FormBuilder, private router: Router) {
    this.Form = this.fb.group({
      name: [''],
      surname: [''],
      hogwartsHouse: [''],
      address: [''],
      owlName: ['']
    });

    this.hogwartsHouse = [
      { name: 'Gryffindor' },
      { name: 'Hufflepuff' },
      { name: 'Slytherin' },
      { name: 'Ravenclaw' }
    ];
  }

  onSubmit() {
    // Handle form submission here
    this.router.navigate(['/tracking']);
  }
}
