import { Component } from '@angular/core';

interface EventItem {
  status?: string;
  date?: string;
  icon?: string;
  color?: string;
  image?: string;
  text?: string; // Add a text property for the event description
}

@Component({
  selector: 'app-tracking',
  templateUrl: './tracking.component.html',
  styleUrls: ['./tracking.component.css']
})
export class TrackingComponent {
  events: EventItem[];

  constructor() {
    this.events = [
      {
        status: 'Adoption Inquiry was sent',
        date: 'just now',
        icon: 'pi pi-send',
        color: '#E3B23C',
        text: 'We have goten your inuiry about adopting a Magical Beast. We will send an owl you as soon as possible'
      },
      {
        status: 'First Check',
        date: 'in a few days',
        icon: 'pi pi-cog',
        color: '#BBB891',
        text: 'We will check if your inquiry is filled out right and we will ask some questions about how your new life with a Magical Beast look like.'
      },
      {
        status: 'Invitation to meet the Beast',
        date: 'in the next week or two',
        icon: 'pi pi-user',
        color: '#DC965A',
        text: 'We will invite you to meet your new companion in person to see if it is a real match.'
      },
      {
        status: 'Adoption',
        date: 'after the meeting if every thing fits',
        icon: 'pi pi-check',
        color: '#A39594',
        text: 'When everything is checked there is nothing stopping your new life with your Beast.'
      }
    ];
  }
}
