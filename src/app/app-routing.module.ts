import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AdoptionComponent} from "./adoption/adoption.component";
import {HomeComponent} from "./home/home.component";
import {ShopComponent} from "./shop/shop.component";
import {FormComponent} from "./form/form.component";
import {TrackingComponent} from "./tracking/tracking.component";

const routes: Routes = [
  {path: 'adoption', component: AdoptionComponent},
  {path: '', component: HomeComponent },
  {path: 'shop', component: ShopComponent},
  {path: 'form', component: FormComponent},
  {path: 'tracking', component: TrackingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],

})
export class AppRoutingModule {}
