import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { AdoptionComponent } from './adoption/adoption.component';
import { ShopComponent } from './shop/shop.component';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { HomeComponent } from './home/home.component';
import { DataViewModule } from 'primeng/dataview';
import {DropdownModule} from "primeng/dropdown";
import {FormsModule} from "@angular/forms";
import {RatingModule} from "primeng/rating";
import {TagModule} from "primeng/tag";
import {PaginatorModule } from 'primeng/paginator';
import { FormComponent } from './form/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { TrackingComponent } from './tracking/tracking.component';
import { TimelineModule } from 'primeng/timeline';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AdoptionComponent,
    ShopComponent,
    HomeComponent,
    FormComponent,
    TrackingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CardModule,
    ButtonModule,
    DataViewModule,
    DropdownModule,
    FormsModule,
    RatingModule,
    TagModule,
    PaginatorModule,
    BrowserModule,
    ReactiveFormsModule,
    InputTextareaModule,
    InputTextModule,
    TimelineModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
